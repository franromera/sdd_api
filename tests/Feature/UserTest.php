<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserTest extends TestCase
{
    private $token;

    function login($credentials = null)
    {
        if (is_null($credentials)) {
            $credentials = ['email' => 'test@fromera.es', 'password' => 'password'];
        }
        $this->token = auth()->attempt($credentials);
    }

    public function test_get_user_invalid_token()
    {
        $response = $this->get('/api/user');
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 401) {
            $this->fail('Error: get user successful');
        }
        $response->assertStatus(200);
    }

    public function test_get_user_successful()
    {
        self::login();
        $response = $this->get('/api/user', ['token' => $this->token]);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error: get user failed');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_invalid_token()
    {
        $data = [
            'token' => $this->token,
            'name' => 'Fran Romera'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 401) {
            $this->fail('Error: update user successful');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_name_successful()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'name' => 'Fran Romera'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error update user: name failed');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_email_unique()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'email' => 'test@fromera.es'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Error update user: email unique failed');
        }
        $response->assertStatus(200);
    }


    public function test_update_user_invalid_email()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'email' => 'test_fromera.es'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Error update user: invalid email');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_password_not_match()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'password' => 'password',
            'old_password' => 'wrong_password'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Error update user: old password not match');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_old_password_not_found()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'password' => 'password'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Error update user: old password not found');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_password_successful()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'password' => 'password',
            'old_password' => 'password'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error update user: password successful');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_email_successful()
    {
        self::login();
        $data = [
            'token' => $this->token,
            'email' => 'email@fromera.es'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error update user: email successful');
        }
        $response->assertStatus(200);
    }

    public function test_update_user_successful()
    {
        self::login(['email' => 'email@fromera.es', 'password' => 'password']);
        $data = [
            'token' => $this->token,
            'email' => 'test@fromera.es',
            'name' => 'Fran Romera',
            'password' => 'password',
            'old_password' => 'password'
        ];
        $response = $this->put('/api/user', $data);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error update user: successful');
        }
        $response->assertStatus(200);
    }

    public function test_user_picture_invalid_token()
    {
        $response = $this->get('/api/user/picture');
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 401) {
            $this->fail('Error generate user picture: successful');
        }
        $response->assertStatus(200);
    }

    public function test_user_picture_successful()
    {
        self::login();
        $response = $this->get('/api/user/picture', ['token' => $this->token]);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error generate user picture: failed');
        }
        $response->assertStatus(200);
    }

    public function test_delete_user_invalid_token()
    {
        $response = $this->delete('/api/user');
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 401) {
            $this->fail('Error delete user: successful');
        }
        $response->assertStatus(200);
    }

    public function test_delete_user_successful()
    {
        self::login();
        $response = $this->delete('/api/user', ['token' => $this->token]);
        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error delete user: failed');
        }
        $response->assertStatus(200);
    }
}
