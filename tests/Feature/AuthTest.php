<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    public function test_register_not_valid_email()
    {
        $data = ['email' => 'not_valid_email', 'name' => 'Francesc Romera', 'password' => 'password'];
        $response = $this->post('/api/auth/register', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Registration error: Not valid email');
        }
        $response->assertStatus(200);
    }

    public function test_register_email_required()
    {
        $data = ['name' => 'Francesc Romera', 'password' => 'password'];
        $response = $this->post('/api/auth/register', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Registration error: Email is required');
        }
        $response->assertStatus(200);
    }

    public function test_register_name_required()
    {
        $data = ['email' => 'name_required@fromera.es', 'password' => 'password'];
        $response = $this->post('/api/auth/register', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Registration error: Name is required');
        }
        $response->assertStatus(200);
    }

    public function test_register_password_required()
    {
        $data = ['email' => 'password_required@fromera.es', 'name' => 'Francesc Romera'];
        $response = $this->post('/api/auth/register', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 400) {
            $this->fail('Registration error: Password is required');
        }
        $response->assertStatus(200);
    }

    public function test_register_successful()
    {
        $data = ['email' => 'test@fromera.es', 'name' => 'Francesc Romera', 'password' => 'password'];
        $response = $this->post('/api/auth/register', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Registration error');
        }
        $response->assertStatus(200);
    }

    public function test_login_unauthorized()
    {
        $data = ['email' => 'test@fromera.es', 'password' => 'wrong_password'];
        $response = $this->post('/api/auth/login', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 401) {
            $this->fail('Error login successful');
        }
        $response->assertStatus(200);
    }

    public function test_login_successful()
    {
        $data = ['email' => 'test@fromera.es', 'password' => 'password'];
        $response = $this->post('/api/auth/login', $data);

        if (isset($response->getData()->data->code) && $response->getData()->data->code != 200) {
            $this->fail('Error login failed');
        }
        $response->assertStatus(200);
    }
}
