<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'namespace' => 'Api',
], function () {

    /**
     * Auth Routes
     */
    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::post('register', ['as' => 'register', 'uses' => 'UserController@store']);

        Route::group([
            'namespace' => 'Auth',
        ], function () {
            Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
        });
    });

    /**
     * User Routes
     */
    Route::group([
        // Authenticate required
        'middleware' => 'auth:api'
    ], function () {
        Route::get('user', ['as' => 'get-user', 'uses' => 'UserController@show']);
        Route::put('user', ['as' => 'update-user', 'uses' => 'UserController@update']);
        Route::delete('user', ['as' => 'delete-user', 'uses' => 'UserController@destroy']);
        Route::get('user/picture', ['as' => 'generate-picture-user', 'uses' => 'UserController@generatePictureUser']);
    });
});