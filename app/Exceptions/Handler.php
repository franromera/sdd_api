<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    private $apiStatusCode = 200;

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //return parent::render($request, $exception);

        $rendered = parent::render($request, $exception);

        /**
         * First, check if the request is from API or other
         */
        if ($request->is('api/*')) {
            if ($exception instanceof ValidationException) {
                // Set status 400
                $rendered->setStatusCode(400);
                // Show errors of validation
                $details = (Array)$exception->errors();
            } else {
                if ($exception instanceof JWTException ||
                    $exception instanceof TokenInvalidException ||
                    $exception instanceof TokenExpiredException ||
                    $exception instanceof AuthenticationException
                ) {
                    // Set status 401
                    $rendered->setStatusCode(401);
                    // Show errors of JWT
                    $details = (String)$exception->getMessage();
                } else {
                    if (env('APP_ENV') !== 'local') {
                        // Rest of exceptions Show title on production
                        $details = (String)$exception->getMessage();
                    } else {
                        // Rest of exceptions, show trace only on development environment
                        $details = (Array)$exception->getTrace();
                    }
                }
            }

            if (env('APP_ENV') !== 'local') {
                $source = [
                    'method' => (String)$request->method(),
                    'pointer' => (String)$request->url()
                ];
            } else {
                $source = [
                    'method' => (String)$request->method(),
                    'pointer' => (String)$request->url(),
                    'parameters' => (Array)$request->only($request->keys())
                ];
            }

            if ($exception->getCode() !== 0) {
                $rendered->setStatusCode($exception->getCode());
            }

            $exception = [
                'source' => $source,
                'title' => (String)$exception->getMessage(),
                'detail' => $details
            ];
            return getResponse($exception, (Int)$rendered->getStatusCode());
        }

        return $rendered;
    }
}
