<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Imagick;
use ImagickPixel;

class UserImageController extends Controller
{
    public function createImage($userId)
    {
        $outputFormat = 'png';
        $dirPath = '/images/users/';
        !is_dir(public_path() . $dirPath) ? mkdir(public_path() . $dirPath, 0755, true) : "";

        // Generamos binario aleatorio a partir de la ID del user, 15 números
        srand($userId);
        $randArray = str_split(sprintf("%015b", rand(0, (pow(2, 15) - 1))));

        $image = new Imagick();
        $image->newImage(5, 5, new ImagickPixel('#ffffff'));

        $pixelIterator = $image->getPixelIterator();
        foreach ($pixelIterator as $y => $pixels) {
            $counter = $y * 3;
            foreach ($pixels as $x => $pixel) {
                if ($randArray[$counter] == 1) {
                    //echo 'Counter: '.$counter.PHP_EOL.' Position: '.$y.','.$x.' -> 1'.PHP_EOL;
                    $pixel->setColor("#0000ff");
                }
                if ($x < 2) {
                    $counter++;
                } else {
                    $counter--;
                }
            }
            $pixelIterator->syncIterator();
        }

        $image->resizeImage(250, 250, Imagick::STRETCH_NORMAL, 0);

        $image->setImageFormat($outputFormat);
        $imagePath = $dirPath . $userId . '.' . $outputFormat;
        $image->writeImage(public_path() . $imagePath);

        return $imagePath;
    }
}
