<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Exception;

class UserController extends Controller
{
    public function show()
    {
        $user = getLoggedUser();
        return getResponse('', 200, 'user', $user);
    }

    public function store(StoreUser $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if ($user) {
            $message = 'The user has been created';
            $status = '200';
        } else {
            throw new Exception('The user could not be created', 400);
        }

        return getResponse($message, $status);
    }

    public function checkIfParamExists($request, $userUpdate, $field)
    {
        if ($request->{$field}) {
            $userUpdate[$field] = $request->{$field};
        }
        return $userUpdate;
    }

    public function update(UpdateUser $request)
    {
        $user = getLoggedUser();

        $userUpdate = [];
        $userUpdate = $this->checkIfParamExists($request, $userUpdate, 'name');
        $userUpdate = $this->checkIfParamExists($request, $userUpdate, 'email');

        if ($request->password && !$request->old_password) {
            throw new Exception('Password old not found', 400);
        } elseif ($request->password && $request->old_password) {
            if (Hash::check($request->old_password, $user->password)) {
                $request['password'] = Hash::make($request->password);
                $userUpdate = $this->checkIfParamExists($request, $userUpdate, 'password');
            } else {
                throw new Exception('Passwords not match', 400);
            }
        }

        $user->update($userUpdate);

        if ($user) {
            $message = 'The user has been updated';
            $status = '200';
        } else {
            throw new Exception('The user could not be updated', 400);
        }

        return getResponse($message, $status);
    }

    public function destroy(Request $request)
    {
        $user = getLoggedUser();
        try {
            if ($user->image != "") {
                file_exists($image = public_path() . $user->image) ? unlink($image) : "";
            }
            $userDelete = $user->delete();

            if ($userDelete) {
                $message = 'The user has been deleted';
                $status = '200';
            }
        } catch (Exception $e) {
            throw new Exception('The user could not be deleted', 400);
        }

        return getResponse($message, $status);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function generatePictureUser(Request $request)
    {
        $user = getLoggedUser();
        $image = (new UserImageController())->createImage($user->id);
        $user->update(['image' => $image]);

        if ($image) {
            $message = 'The user image has been created';
            $status = '200';
        } else {
            throw new Exception('User image could not be created', 400);
        }

        return getResponse($message, $status);
    }
}
