<?php
/**
 * Created by PhpStorm.
 * User: frome
 * Date: 23/11/2019
 * Time: 17:41
 */

namespace App\Helpers;

class UserHelper
{
    public static function getLoggedUser()
    {
        return auth('api')->user();
    }

}