<?php
/**
 * Created by PhpStorm.
 * User: frome
 * Date: 23/11/2019
 * Time: 17:41
 */

namespace App\Helpers;

class ResponseHelper
{

    public static function getResponse($message, $statusCode, $model = null, $collection = null)
    {
        $data['code'] = $statusCode;
        $statusCode == 200 ? $data['status'] = 'success' : $data['status'] = 'error';
        $data['message'] = $message;
        if (!is_null($model) && !is_null($collection)) {
            $data['type'] = $model;
            $data['id'] = $collection->id;
            $data['attributes'] = $collection;
        }
        $return = [
            'data' => $data,
        ];
        return response()->json($return, 200)
            ->withHeaders(['Content-Type' => "application/json"]);
    }

}