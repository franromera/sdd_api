<?php
/**
 * Created by PhpStorm.
 * User: frome
 * Date: 23/11/2019
 * Time: 17:40
 */

use App\Helpers\UserHelper;
use App\Helpers\ResponseHelper;

if (!function_exists('getLoggedUser')) {
    function getLoggedUser()
    {
        return UserHelper::getLoggedUser();
    }
    function getResponse($message, $statusCode, $model = null, $collection = null)
    {
        return ResponseHelper::getResponse($message, $statusCode, $model, $collection);
    }
}
