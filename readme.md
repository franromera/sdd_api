<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

FORMAT: 1A

HOST: http://localhost/api

Documentación en Apiary: https://francescromerarodriguez.docs.apiary.io/#

Se utiliza la librería de Imagick para generar la imagen.

# SDD Api

Prueba técnica para SDD


## Register [/auth/register]

### Register [POST]

@Authorization: no needed

+ Request

    + Headers

            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json

    + Attributes

        + name: Francesc Romra (string, required) - Name
        + email: hola@fromera.es (string, required) - Email
        + password: password (string, required) - Password

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

            {
                "data": {
                    "code": "200",
                    "status": "success",
                    "message": "The user has been created"
                }
            }


## Login [/auth/login]

### Login [POST]

@Authorization: no needed

+ Request

    + Headers
    
            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json

    + Attributes

        + email: hola@fromera.es (string, required) - Email
        + password: password (string, required) - Password

+ Response 200

    + Headers

            Content-Type: application/json

    + Body

            {
                "data": {
                    "code": "200",
                    "status": "success",
                    "message": {
                        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NkZF9hcGlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU3NDUzNzE3OSwiZXhwIjoxNTc0NTQwNzc5LCJuYmYiOjE1NzQ1MzcxNzksImp0aSI6ImZTbGdva2o1UWFZMXpNeUEiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.x20SigQ9IFF8KkA3QMDKxRnaU8fbaFB5X1tXlMnIoZk",
                        "token_type": "bearer",
                        "expires_in": 3600
                    }
                }
            }

## User [/user]

### User [GET]

@Authorization: required


+ Request

    + Headers

            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json
            Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NkZF9hcGlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU3NDUzNzE3OSwiZXhwIjoxNTc0NTQwNzc5LCJuYmYiOjE1NzQ1MzcxNzksImp0aSI6ImZTbGdva2o1UWFZMXpNeUEiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.x20SigQ9IFF8KkA3QMDKxRnaU8fbaFB5X1tXlMnIoZk

+ Response 200

    + Headers

            Content-Type: application/json

    + Body
            {
                "data": {
                    "code": 200,
                    "status": "success",
                    "message": "",
                    "type": "user",
                    "id": 2,
                    "attributes": {
                        "id": 2,
                        "name": "Francesc Romera",
                        "email": "test@fromera.es",
                        "image": "/images/users/2.png",
                        "created_at": "2019-11-23 19:25:10",
                        "updated_at": "2019-11-23 19:27:06"
                    }
                }
            }

### User [PUT]

@Authorization: required

+ Request

    + Headers

            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json
            Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NkZF9hcGlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU3NDUzNzE3OSwiZXhwIjoxNTc0NTQwNzc5LCJuYmYiOjE1NzQ1MzcxNzksImp0aSI6ImZTbGdva2o1UWFZMXpNeUEiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.x20SigQ9IFF8KkA3QMDKxRnaU8fbaFB5X1tXlMnIoZk

    + Attributes

        + name: Fran Romera (string, optional) - Name
        + email: hello@fromera.es (string, optional) - Email
        + password: password (string, optional) - Password
        + old_password: password (string, optional) - Password
        
+ Response 200

    + Headers

            Content-Type: application/json

    + Body
            {
                "data": {
                    "code": "200",
                    "status": "success",
                    "message": "The user has been updated"
                }
            }


### User [DELETE]

@Authorization: required

+ Request

    + Headers

            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json
            Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NkZF9hcGlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU3NDUzNzE3OSwiZXhwIjoxNTc0NTQwNzc5LCJuYmYiOjE1NzQ1MzcxNzksImp0aSI6ImZTbGdva2o1UWFZMXpNeUEiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.x20SigQ9IFF8KkA3QMDKxRnaU8fbaFB5X1tXlMnIoZk

+ Response 200

    + Headers

            Content-Type: application/json

    + Body
            {
                "data": {
                    "code": "200",
                    "status": "success",
                    "message": "The user has been deleted"
                }
            }


## User Picture [/user/picture]

### User Picture [GET]

@Authorization: required

+ Request

    + Headers

            Content-Type: multipart/form-data
            Cache-Control: no-cache
            Connection: keep-alive
            Accpet: application/json
            Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NkZF9hcGlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU3NDUzNzE3OSwiZXhwIjoxNTc0NTQwNzc5LCJuYmYiOjE1NzQ1MzcxNzksImp0aSI6ImZTbGdva2o1UWFZMXpNeUEiLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.x20SigQ9IFF8KkA3QMDKxRnaU8fbaFB5X1tXlMnIoZk

+ Response 200

    + Headers

            Content-Type: application/json

    + Body
            {
                "data": {
                    "code": "200",
                    "status": "success",
                    "message": "The user image has been created"
                }
            }

